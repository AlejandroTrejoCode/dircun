<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Dircun</title>
    <link rel="stylesheet" href="">
    <!-- Hojas de estilos -->
    <link rel="stylesheet" href="css/styleEstablecimiento.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--<link rel="stylesheet" href="css/generics.css">-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
    <link href="animate.css" rel="stylesheet">
</head>
<body>
<div class="screen">
<div class="container">
<!--Container-->
<div class="contentEstablecimiento col-xs-12 col-md-8"> 
<!--Header-->
    <div class="headerEstablecimiento"><h1>Gran Caribe Resort</h1></div> 
<!--Image-->
    <div class="imagesEstablecimiento col-xs-12 col-md-6 img-responsive"> 
        <img src="https://i0.bookcdn.com/data/Photos/OriginalPhoto/1941/194138/194138605/Gran-Caribe-Resort-photos-Exterior.JPEG" alt="Cancún es bello">
    </div>
<!--Description-->
    <div class="descriptionEstablecimiento col-xs-12 col-md-6"> 
        <p>
            Este exclusivo complejo con todo incluido, ubicado en la costa del Caribe, se encuentra a 1,8 km del Acuario Interactivo de Cancún y a 6 km del Museo Maya de Cancún. 
        </p>
    </div>

<!--Maps-->
    <div class="mapEstablecimiento col-xs-12 col-md-12"> 
    
    <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1ses-419!2s!4v1480391179981!6m8!1m7!1sF%3A-b2PX_OCGK3I%2FWCBtR9x1l1I%2FAAAAAAAAL2c%2F-oAq_O1OPAADHh_raOU52d8qwmwXu5pqwCLIB!2m2!1d21.1205274!2d-86.7552889!3f53.55!4f1.2199999999999989!5f0.7820865974627469" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
  
     
        
    </div>
<!--Contact-->
    <div class="contactEstablecimiento col-xs-12 col-md-12"> 
       <div class="contact col-xs-3 col-md-3"> 
           <label>Contacto</label>
       </div>
    <!--<<div class="email col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-envelope"></span>
            <email>contacto@contacto.com</email>
        </div>-->
        <div class="telephone col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-earphone"></span>
            <tel>01 998 848 7800</tel>
        </div>
        <div class="facebook col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-thumbs-up"></span>
            <a href="https://www.facebook.com">Facebook</a>
        </div>
        <div class="twitter col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-heart"></span>
            <a href="https://www.twitter.com">Twitter</a>
        </div>       
    </div>
</div> 
</div>
</div>


<div class="scrReviews col-xs-12 col-md-12">
   <div class="contReviews col-xs-12 col-md-12">
       <div class="reviews col-xs-11 col-md-11">
       <div class="headerReviews"><h1>Reviews</h1></div> 
        <div class="textReviews col-xs-10 col-md-10">
           <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="5"></div>
        </div>
       </div>
   </div>
</div>

</body>
</html>