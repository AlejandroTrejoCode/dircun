-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-11-2016 a las 15:54:09
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prtn`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCat` int(11) NOT NULL,
  `categ` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCat`, `categ`) VALUES
(1, 'Educacion'),
(2, 'Restaurantes'),
(3, 'Centros Comerciales'),
(4, 'Compras'),
(5, 'Hoteles'),
(6, 'Atracciones');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `establecimientos`
--

CREATE TABLE `establecimientos` (
  `idEs` int(11) NOT NULL,
  `idEm` int(11) DEFAULT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Direccion` varchar(50) DEFAULT NULL,
  `Coords` varchar(100) DEFAULT NULL,
  `Tel` varchar(15) DEFAULT NULL,
  `Cate` int(11) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `webPage` varchar(50) DEFAULT NULL,
  `Horario` varchar(100) DEFAULT NULL,
  `socialPage` varchar(50) DEFAULT NULL,
  `Descrip` varchar(200) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `establecimientos`
--

INSERT INTO `establecimientos` (`idEs`, `idEm`, `Nombre`, `Direccion`, `Coords`, `Tel`, `Cate`, `Email`, `webPage`, `Horario`, `socialPage`, `Descrip`, `img`) VALUES
(1, 1, 'Mc donals plaza las americas', 'Plaza las americas local 3a', '21.146639, -86.824136', '88858825', 2, 'mcamericas@mcdonals.com', 'mcdonals.com', '9:00am-11:00pm,7:00am-11:00pm', 'facebook.com/mclasamericas', 'Vende hamburguesas', 'img/estab/Mc donals plaza las americas.jpg'),
(2, 6, 'Parque de las palapas', '5 Alcatraces, Sm 22, Benito Juárez, 77500', '21.1611887,-86.8277701', '01 998 577 2722', 6, 'palapas@gmail.com', 'www.palapas.cpm', 'Todo el día', NULL, 'El Parque de las Palapas es un gran parque en la zona continental de la ciudad de Cancún, que sus habitantes utilizan como punto de reunión.', 'img/Palapas.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `idPay` int(11) NOT NULL,
  `idEm` int(11) DEFAULT NULL,
  `idEs` int(11) DEFAULT NULL,
  `Monto` int(50) DEFAULT NULL,
  `Promo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUs` int(11) NOT NULL,
  `nomUs` varchar(100) DEFAULT NULL,
  `Pass` varchar(20) DEFAULT NULL,
  `tel` varchar(12) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `razon` varchar(20) DEFAULT NULL,
  `descr` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUs`, `nomUs`, `Pass`, `tel`, `email`, `cp`, `razon`, `descr`, `logo`) VALUES
(1, 'Mc donals Cancun', 'mccancun', '8888123', 'mccancun@mcdonals.com', 77511, 'S.A de C.V', 'Vende ratas', 'img/logs/mcdonals.png'),
(2, 'Cinepolis Cancun', 'cinecun', '8888123', 'cinecun@cinepolis.com', 77511, 'S.A de C.V', 'Estafadores', 'img/logs/cine.png'),
(5, 'walmart', 'xd', '5555', 'wmcun@wallmart.com', 77510, 'S.A de C.v', 'Vende de todo', 'img/logs/walmart.png'),
(6, 'Parque de las palapas', 'palapas', '9981244364', 'palapas@gmail.com', 77510, 'SA de CV', 'El Parque de las Palapas es un gran parque en la zona continental de la ciudad de Cancún, que sus habitantes utilizan como punto de reunión.', 'img/logs/null.png');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCat`);

--
-- Indices de la tabla `establecimientos`
--
ALTER TABLE `establecimientos`
  ADD PRIMARY KEY (`idEs`),
  ADD KEY `establecimientos_ibfk_1` (`idEm`),
  ADD KEY `establecimientos_ibfk_2` (`Cate`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`idPay`),
  ADD KEY `pagos_ibfk_1` (`idEs`),
  ADD KEY `pagos_ibfk_2` (`idEm`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUs`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `establecimientos`
--
ALTER TABLE `establecimientos`
  MODIFY `idEs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `idPay` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `establecimientos`
--
ALTER TABLE `establecimientos`
  ADD CONSTRAINT `establecimientos_ibfk_1` FOREIGN KEY (`idEm`) REFERENCES `usuarios` (`idUs`),
  ADD CONSTRAINT `establecimientos_ibfk_2` FOREIGN KEY (`Cate`) REFERENCES `categorias` (`idCat`);

--
-- Filtros para la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD CONSTRAINT `pagos_ibfk_1` FOREIGN KEY (`idEs`) REFERENCES `establecimientos` (`idEs`),
  ADD CONSTRAINT `pagos_ibfk_2` FOREIGN KEY (`idEm`) REFERENCES `usuarios` (`idUs`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
