<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Buscar: Hoteles - Dircun</title>
    <meta name="author" content="Reef Studios">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">   
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
    <!-- Hojas de estilos -->            
    <link rel="stylesheet" href="css/styleBusquedas.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">
</head>
<body>
    <div class="searchScreen">
        <div class="logo">
            <a href="index.php"><img src="img/DirCun.gif" alt="" class="img-responsive"></a>
        </div>
        <div class="serchContent">
            <div class="searchHead">
                <h2>Resultados para: <span> Hoteles</span></h2>
                <hr>
            </div>
            <div class="searchContainer">
                <div class="element wow fadeInUp" data-wow-delay="0.2s">
                    <div class="image">
                        <img src="img/FiestaAmericana.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Grand Fiesta Americana Coral</h2>
                        <p>Este hotel lujoso en la playa está situado en una península, a 3,7 km del acuario interactivo de Cancún, a 8 km del Museo Maya de la ciudad y a 17 km del parque acuático Wet'n Wild. 
                        Las suites son amplias y tienen suelos de mármol, balcones privados, vistas al mar, televisiones de pantalla plana y Wi-Fi gratis. Las opciones superiores también cuentan con terrazas, mientras que las habitaciones Club disponen de acceso a un salón. Algunas suites incluyen jacuzzis, comedores y cocinas completas. Servicio de habitaciones disponible.
                        <br><a href="GranCaribeResort.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.4s">
                    <div class="image">
                        <img src="img/Sunset.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Sunset Royal Beach Resort</h2>
                        <p> Este complejo con todo incluido tranquilo se encuentra en una playa de arena blanca, a 2,6 km del Acuario Interactivo de Cancún y a 8 km del Museo Maya de Cancún. 
                        Las habitaciones son luminosas y cuentan con terrazas o balcones con vistas al mar Caribe, televisiones de pantalla plana, cajas fuertes, cocinas con microondas, minibares, cafeteras y teteras.
                        <br> <a href="SunsetRoyalBeachResort.php" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.6s">
                    <div class="image">
                        <img src="img/LeBlanc.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Le Blanc Spa Resort</h2>
                        <p> Este complejo con todo incluido solo admite adultos y se encuentra en la playa, a 8 km del Museo Maya de Cancún y a 17 km del parque acuático Wet'n Wild Cancún. 
                        Las habitaciones, amplias y refinadas, tienen balcones con vistas al mar, Wi-Fi gratis, televisiones de pantalla plana con conexiones multimedia, escritorios, zonas de descanso y bañeras de hidromasaje.
                        <br> <a href="LeBlancSpaResort.php" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.6s">
                    <div class="image">
                        <img src="img/GranPark.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Grand Park Royal Cancun</h2>
                        <p> Este complejo refinado con todo incluido se encuentra en una playa privada en el mar Caribe, a 2 km del MUSA, el museo subacuático de arte. 
                        Las habitaciones son luminosas y disponen de suelos de mármol y balcones con vistas al mar, televisiones de pantalla plana, minibares gratuitos, cafeteras y cajas fuertes.
                        <br> <a href="Grand-Park-Royal-Cancun-Caribe.php" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>  
                <div class="endSearch">
                    <br>
                    <h2>- Fin de las busquedas -</h2>
                    <br>
                </div>                
            </div>            
        </div>        
    </div>
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="dist/wow.js"></script>
    <script>
        new WOW().init();
    </script> 
</body>
</html>