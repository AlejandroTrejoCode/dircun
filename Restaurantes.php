<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Buscar: Restaurantes - Dircun</title>
    <meta name="author" content="Reef Studios">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">   
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
    <!-- Hojas de estilos -->            
    <link rel="stylesheet" href="css/styleBusquedas.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">
</head>
<body>
    <div class="searchScreen">
        <div class="logo">
            <a href="index.php"><img src="img/DirCun.gif" alt="" class="img-responsive"></a>
        </div>
        <div class="serchContent">
            <div class="searchHead">
                <h2>Resultados para: <span> Restaurantes</span></h2>
                <hr>
            </div>
            <div class="searchContainer">
                <div class="element wow fadeInUp" data-wow-delay="0.2s">
                    <div class="image">
                        <img src="img/JugoDeLimon.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Jugo De Limón Marisquería</h2>
                        <p>Disfruta de la mas amplia variedad de cócteles, ceviches y platillos de mariscos en un ambiente tropical cálido, con una magnifica vista de la laguna. 
                        Ven con tu familia y disfruta mientras comes de la vista a un delfinario y un tiburonario, tus hijos lo amaran.
                        <br><a href="" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.4s">
                    <div class="image">
                        <img src="img/Hacienda.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Hacienda El Mortero</h2>
                        <p> En el estado de Durango, al norte de México, se encuentra la hacienda "El Mortero" como un espléndido testimonio de nuestra herencia cultural. Su construcción actualmente es considerada como una joya de la arquitectura del siglo XVII y es dignamente representada por nuetro restaurante Hacienda El Mortero, construido como una réplica del casco de la obra original.
                        <br> <a href="HaciendaElMortero.php" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.6s">
                    <div class="image">
                        <img src="img/Chunito.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Chuchito Pérez</h2>
                        <p>En Chuchito Perez experimentaras una exclusiva fusion de comida Mexicana - Oriental con un estilo y sabor únicos en cada platillo, combinado con la espectacular vista a la laguna y party center de Cancún, es el lugar ideal para amigos, familias y parejas, cualquier día de la semana.
                        <br><a href="ChuchitoPerezCancun.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.8s">
                    <div class="image">
                        <img src="img/Tilon.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>El Tilón Jarocho</h2>
                        <p>El sabor de Veracruz auténtico con su arroz a la Tumbada, sus mariscos, ceviches, caldo de mariscos y su variedad de pescados, en todo lo largo de la Av. López Portillo, domina este restaurante. 
                        <br><a href="ElTilonJarocho.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="endSearch">
                    <br>
                    <h2>- Fin de las busquedas -</h2>
                    <br>
                </div>                
            </div>            
        </div>        
    </div>
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="dist/wow.js"></script>
    <script>
        new WOW().init();
    </script> 
</body>
</html>