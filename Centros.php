<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Buscar: Centros Comerciales - Dircun</title>
    <meta name="author" content="Reef Studios">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">   
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
    <!-- Hojas de estilos -->            
    <link rel="stylesheet" href="css/styleBusquedas.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">
</head>
<body>
    <div class="searchScreen">
        <div class="logo">
            <a href="index.php"><img src="img/DirCun.gif" alt="" class="img-responsive"></a>
        </div>
        <div class="serchContent">
            <div class="searchHead">
                <h2>Resultados para: <span> Centros Comerciales</span></h2>
                <hr>
            </div>
            <div class="searchContainer">
                <div class="element wow fadeInUp" data-wow-delay="0.2s">
                    <div class="image">
                        <img src="img/Mandala.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Mandala Beach Club</h2>
                        <p>Las mejores fiestas en la playa ocurren en un solo lugar: Mandala Beach Club, el mar Caribe, bikinis, bebidas tropicales * Djs hará que su día en la playa sea una experiencia inolvidable el restaurante ofrece una variedad de cursos para disfrutar acompañados de sus bebidas favoritas Con la playa como fondo.
                        <br><a href="MandalaBeachClub.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.4s">
                    <div class="image">
                        <img src="img/LaIsla.png" alt="">
                    </div>
                    <div class="title">
                        <h2>Plaza La Isla Cancun</h2>
                        <p> La Isla Cancún Shopping Village cuenta con la mejor mezcla de giros en sus 162 locales: las principales marcas internacionales, nacionales y regionales, restaurantes y entretenimiento. Ganador del premio ICSC, es el centro comercial más visitado de la zona hotelera de Cancún y el de mayor reconocimiento a nivel internacional en nuestro país. 
                        <br> <a href="PlazaLaIslaCancun.php" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>                
                <div class="endSearch">
                    <br>
                    <h2>- Fin de las busquedas -</h2>
                    <br>
                </div>                
            </div>            
        </div>        
    </div>
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="dist/wow.js"></script>
    <script>
        new WOW().init();
    </script> 
</body>
</html>