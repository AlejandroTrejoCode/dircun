<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Dircun</title>
    <link rel="stylesheet" href="">
    <!-- Hojas de estilos -->
    <link rel="stylesheet" href="css/styleEstablecimiento.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--<link rel="stylesheet" href="css/generics.css">-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
    <link href="animate.css" rel="stylesheet">
</head>
<body>
<div class="screen">
<div class="container">
<!--Container-->
<div class="contentEstablecimiento col-xs-12 col-md-8"> 
<!--Header-->
    <div class="headerEstablecimiento"><h1>Playa Langosta</h1></div> 
<!--Image-->
    <div class="imagesEstablecimiento col-xs-12 col-md-6 img-responsive"> 
        <img src="img/PlayaLangosta.jpg">
    </div>
<!--Description-->
    <div class="descriptionEstablecimiento col-xs-12 col-md-6"> 
        <p>
            Playa Langosta es una de las playas más populares y familiares de Cancún. Debido a sus aguas calmas es una de las mejores playas para el nado y para visitar con niños pequeños. Por otra parte, al no tener grandes olas, no es una playa apta para el surf, pero sí para los deportes de playa como el fútbol y voleibol playeros.
        </p>
    </div>

<!--Maps-->
    <div class="mapEstablecimiento col-xs-12 col-md-12"> 
    
    <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1ses-419!2smx!4v1480394958729!6m8!1m7!1sdajZWDEKNH23nBw8bOv0rg!2m2!1d21.14441123445059!2d-86.78123204840634!3f8.839301018072796!4f-5.424803040002928!5f0.7820865974627469" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
  
     
        
    </div>
<!--Contact-->
    <div class="contactEstablecimiento col-xs-12 col-md-12"> 
       <div class="contact col-xs-3 col-md-3"> 
           <label>Contacto</label>
       </div>
    <!--<<div class="email col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-envelope"></span>
            <email>contacto@contacto.com</email>
        </div>-->
        <div class="telephone col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-earphone"></span>
            <tel>01 998 840 4299</tel>
        </div>
        <div class="facebook col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-thumbs-up"></span>
            <a href="https://www.facebook.com">Facebook</a>
        </div>
        <div class="twitter col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-heart"></span>
            <a href="https://www.twitter.com">Twitter</a>
        </div>       
    </div>
</div> 
</div>
</div>


<div class="scrReviews col-xs-12 col-md-12">
   <div class="contReviews col-xs-12 col-md-12">
       <div class="reviews col-xs-11 col-md-11">
       <div class="headerReviews"><h1>Reviews</h1></div> 
        <div class="textReviews col-xs-10 col-md-10">
           <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="5"></div>
        </div>
       </div>
   </div>
</div>

</body>
</html>