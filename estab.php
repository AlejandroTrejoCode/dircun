<?php 
  require 'funtions/conexion.php';
  $id=$_GET['id'];
  $query = $connection -> prepare(
  'SELECT establecimientos.idEs, establecimientos.Nombre, establecimientos.Direccion, establecimientos.Coords, establecimientos.Tel, establecimientos.Email, establecimientos.webPage, establecimientos.Horario, establecimientos.socialPage, establecimientos.Descrip, establecimientos.img, usuarios.nomUs as empresa, usuarios.logo as logo,categorias.categ as categoria from categorias INNER JOIN usuarios,establecimientos where establecimientos.idEm=usuarios.idUs AND establecimientos.Cate=categorias.idCat and idEs=:id');

        $query->bindValue(':id', intval($id), PDO::PARAM_INT);


  $query -> execute(array(
  'id' => $id));
  
  $data = $query -> fetch();
  if (empty($data)) {
    header('location index.php');
  } else {
    echo '<script> var cor="'.$data['Coords'].'";</script>'; 
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Custom Street View panorama tiles</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #street-view {
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div id="street-view"></div>
    <script>

var panorama;
var index=cor.indexOf(",");
var x=cor.substring(0,index);
var y=cor.substring(index+1,cor.legth);
var nx=parseFloat(x);
var ny=parseFloat(y);
// StreetViewPanoramaData of a panorama just outside the Google Sydney office.
var outsideGoogle;

// StreetViewPanoramaData for a custom panorama: the Google Sydney reception.
function getReceptionPanoramaData() {
  return {
    location: {
      pano: 'reception',  // The ID for this custom panorama.
      description: 'Google Sydney - Reception',
      latLng: new google.maps.LatLng(nx, ny)
    },
    links: [{
      heading: 195,
      description: 'Exit',
      pano: outsideGoogle.location.pano
    }],
    copyright: 'Imagery (c) 2010 Google',
    tiles: {
      tileSize: new google.maps.Size(1024, 512),
      worldSize: new google.maps.Size(2048, 1024),
      centerHeading: 105,
      getTileUrl: function(pano, zoom, tileX, tileY) {
        return 'images/' +
            'panoReception1024-' + zoom + '-' + tileX + '-' + tileY + '.jpg';
      }
    }
  };
}

function initPanorama() {
  panorama = new google.maps.StreetViewPanorama(
      document.getElementById('street-view'),
      {
        pano: outsideGoogle.location.pano,
        // Register a provider for our custom panorama.
        panoProvider: function(pano) {
          if (pano === 'reception') {
            return getReceptionPanoramaData();
          }
        }
      });

  // Add a link to our custom panorama from outside the Google Sydney office.
  panorama.addListener('links_changed', function() {
    if (panorama.getPano() === outsideGoogle.location.pano) {
      panorama.getLinks().push({
        description: 'Google Sydney',
        heading: 25,
        pano: 'reception'
      });
    }
  });
}

function initialize() {
  // Use the Street View service to find a pano ID on Pirrama Rd, outside the
  // Google office.
  var streetviewService = new google.maps.StreetViewService;
  streetviewService.getPanorama(
      {location: {lat: nx, lng: ny}},
      function(result, status) {
        if (status === google.maps.StreetViewStatus.OK) {
          outsideGoogle = result;
          initPanorama();
        }
      });
}

    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWD3SIg_Ya0n_0tTsCt1Dm-vMnOTAqyWU&signed_in=true&callback=initialize">
    </script>
  </body>
</html>