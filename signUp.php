<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Sign Up</title>
        <meta name="author" content="Reef Studios">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">        
        <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
        <!-- Hojas de estilos -->
        <link rel="stylesheet" href="css/styleSignUp.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <!--<link rel="stylesheet" href="css/generics.css">-->
        <link rel="stylesheet" href="fonts/flaticon.css">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,700,900" rel="stylesheet">
        <link href="animate.css" rel="stylesheet">
</head>
<body>
    <div class="signupScreen">
        <div class="signupContainer">
            <div class="container">
                <div class="register col-md-8">
                    <div class="registerHead">
                        <img src="img/DirCun.gif" class="img-responsive" alt="">
                    </div>
                    <div class="registerForm">
                        <h2>¡Comencemos!</h2>
                        <form action="actions/upload.php" method="post">
                            <input type="text" placeholder="Name" name="nom">
                            <input type="email" placeholder="Email" name="email">
                            <input type="password" placeholder="Password" name="pass">
                            <input type="password" placeholder="Repeat password" name="pass2">
                            <input type="tel" placeholder="Telephone number" name="tel">
                            <input type="Text" placeholder="Codigo postal" name="cp">
                            <input type="Text" placeholder="Descripcion" name="descr">
                            <input type="Text" placeholder="Razon social S.A de C.v" name="razon">
                            <input type="file" value="Añadir logo" name="uploaded"></input>
                            <input type="submit" value="Crear Cuenta" id="Enviar">
                        </form>
                    </div>
                </div>
                <div class="image col-md-4">
                    <div class="imageHead">
                        <h2>¡Eres parte!</h2>
                        <p>Estás a unos pasos de dejar tu marca en Cancún, muéstrale a la gente que ofreces.</p> 
                    </div> 
                </div>
            </div>
        </div>
    </div>
</body>
</html>