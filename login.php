<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dircun - Login</title>
    <meta name="author" content="Reef Studios">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">        
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
    <link rel="stylesheet" href="css/styleLogin.css">
    <link rel="stylesheet" href="css/bootstrap.css">        
    <link rel="stylesheet" href="fonts/flaticon.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
    <link href="animate.css" rel="stylesheet">
</head>
<body>
   <!-- Contenedor Principal -->
    <div class="loginScreen">
        <div class="loginContainer">            
            <div class="container">
                <div class="access col-md-8">
                    <div class="accessHead">
                        <img src="img/DirCun.gif" class="img-responsive" alt="">
                    </div>
                    <div class="accessForm">
                        <h2>¡Buen día!</h2>
                        <p>Este día será excelente</p>
                        <form action="actions/login.php" method="post">
                           <label for="">Correo:</label>
                            <input type="email" placeholder="" name="user" required>
                            <label for="">Contraseña:</label>
                            <input type="password" placeholder="" name="pass" required>
                            <div class="Enviar">
                                <input type="submit" value="Iniciar sesión" id="Enviar">
                                <p>¿Olvido su contraseña?</p>
                            </div>                            
                        </form>
                    </div>
                    <div class="accessFooter">
                        <p>¿No tienes cuenta? - <a href="signUp.php">  Regístrate</a></p>
                    </div>
                </div>
                <div class="image col-md-4">
                    <div class="imageHead">
                        <h2>Cancún</h2>
                        <p>Cancún es uno de los destinos favoritos a nivel mundial que se encuentra ubicado en el sureste de México en el estado de Quintana Roo.</p> 
                    </div>             
                </div>
            </div>            
        </div>
    </div>    
</body>
</html>