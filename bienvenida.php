<?php
    require 'funtions/conexion.php';
    session_start();
    if(isset($_SESSION['id']))
    {
        $id = $_SESSION['id'];
        $query = $connection -> prepare(
	'SELECT establecimientos.idEs, establecimientos.Nombre, usuarios.nomUs, usuarios.logo, establecimientos.img, establecimientos.Direccion FROM usuarios inner join establecimientos WHERE establecimientos.idEm=usuarios.idUs AND establecimientos.idEm=:id');

        $query->bindValue(':id', intval($id), PDO::PARAM_INT);


	$query -> execute(array(
	'id' => $id));
	
	$data = $query -> fetchAll();

    }
    else
    {
        header('Location: ../index.php');
        die;
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Dircun - ¡Bienvenido!</title>
    <link rel="stylesheet" href="css/styleBienvenida.css">
    <!-- Hojas de estilos 
    <link rel="stylesheet" href="css/styleBienvenida.css">-->
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--<link rel="stylesheet" href="css/generics.css">-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
    <link href="animate.css" rel="stylesheet">
</head>
<body>
<div class="title">
    <h2>Tus establecimientos</h2>
</div>
<?php 
    if(empty($data)){
        header('location: addPlace.php');
    }
    else{
        echo '<img class="logoimg" src="'.$data[0]['logo'].'"/>';
        foreach($data as $ndata)
        {
        echo '<a href="#"> <div class="estab">'.$ndata['Nombre'].'</div> </a>';
        echo '<a href="pruebas.php?id='.$ndata['idEs'].'"> <div class="estab">'.$ndata['Direccion'].'</div> </a>';
        echo '<img class="imag" src="'.$ndata['img'].'"/>';
        #echo '<div class="empresa">'.$ndata['nomUs'].'</div>';
        }
    }
?>

<!--Container
<div class="screenWelcome"> 

<div class="containerWelcome">
    <div class="contentWelcome col-xs-11 col-md-8"> 

    <div class="headerWelcome"><h1>Bienvenido</h1></div> 

    <div class="placesWelcome col-xs-2 col-md-2"> 
    <div class="titlePlaces">
        <label>Mis lugares</label>
    </div>
    <div class="myPlaces">
        <a href="">Walmart</a><br>
        <a href="">Sam's Club</a><br>
        <a href="">Superama</a><br>
    </div>
    <div class="addPlace">
        <a href="addPlace.php" type="button" class="btn btn-default" aria-label="Left Align" id="Enviar">
            <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
        </a>
    </div>
    </div>

    <div class="mapWelcome col-xs-10 col-md-10"> 
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.2811992401503!2d-86.9087486854458!3d21.180985485916107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4c2c0162fb4071%3A0xc881c60dfcc845c!2sUniversidad+Polit%C3%A9cnica+de+Quintana+Roo!5e0!3m2!1ses-419!2smx!4v1479573890259" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <div class="dataWelcome">
       <div class="infogral col-xs-6 col-md-6">
      <label for="">Información general</label>
        <div class="form-group">
            <input type="text" class="form-control" id="Name" placeholder="Nombre">  	
        </div>
               
        <div class="form-group">
            <input type="text" class="form-control" id="Address" placeholder="Dirección">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" id="Type" placeholder="Tipo">
        </div>

        <div class="form-group">
            <input type="tel" class="form-control" id="Telephone" placeholder="Núm. telefónico">
        </div>
  	    	                
        <div class="form-group">
            <input type="url" class="form-control" id="Website" placeholder="Website">
        </div>
        
        <div class="form-group">
            <input type="url" class="form-control" id="Facebook" placeholder="Facebook">
        </div>
        
        <div class="form-group">
            <input type="url" class="form-control" id="Twitter" placeholder="Twitter">
        </div>
        

        </div>
       </div>
       
        <div class="horarioDescripcion col-xs-6 col-md-6">
        <label for="">Decripción</label>
        <textarea class="form-control" rows="6" placeholder="¿Qué hace diferente a tu establecimiento?... Escríbelo en menos de 6 líneas."></textarea>
        <label for="">Apertura</label>
        <div class="form-group">
            <input type="time" class="form-control" id="Open" placeholder="Abierto">
        </div>
        
        <label for="">Cierre</label>
        <div class="form-group">
            <input type="time" class="form-control" id="Closed" placeholder="Cerrado">
        </div>
     
        <br>
        <button type="submit" class="btn btn-default" id="Enviar">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
        </button>
        </div>
        
        
    </div>

    <div class="reviewsWelcome">
        
    </div>
</div>
</div>

</div>-->

</body>
</html>