<?php 
  require 'funtions/conexion.php';
  $id=$_GET['id'];
  $query = $connection -> prepare(
  'SELECT establecimientos.idEs, establecimientos.Nombre, establecimientos.Direccion, establecimientos.Coords, establecimientos.Tel, establecimientos.Email, establecimientos.webPage, establecimientos.Horario, establecimientos.socialPage, establecimientos.Descrip, establecimientos.img, usuarios.nomUs as empresa, usuarios.logo as logo,categorias.categ as categoria from categorias INNER JOIN usuarios,establecimientos where establecimientos.idEm=usuarios.idUs AND establecimientos.Cate=categorias.idCat and idEs=:id');

        $query->bindValue(':id', intval($id), PDO::PARAM_INT);


  $query -> execute(array(
  'id' => $id));
  
  $data = $query -> fetch();
  if (empty($data)) {
    header('location index.php');
  } else {
    echo '<script> var cor="'.$data['Coords'].'";</script>'; 
  }
?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Dircun</title>
    <link rel="stylesheet" href="">
    <!-- Hojas de estilos -->
    <link rel="stylesheet" href="css/styleEstablecimiento.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--<link rel="stylesheet" href="css/generics.css">-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
    <link href="animate.css" rel="stylesheet">
</head>
<body>
<div class="screen">
<div class="container">
<!--Container-->
<div class="contentEstablecimiento col-xs-12 col-md-8"> 
<!--Header-->
    <?php echo '<div class="headerEstablecimiento"><h1>'.$data['Nombre'].'</h1></div>' ; ?>
<!--Image-->
    <div class="imagesEstablecimiento col-xs-12 col-md-6 img-responsive"> 
        <?php echo '<img src="'.$data['img'].'" alt="Cancún es bello">';?>
    </div>
<!--Description-->
    <div class="descriptionEstablecimiento col-xs-12 col-md-6"> 
        <?php echo '<p>'.$data['Descrip'].'</p>'; ?>
    </div>

<!--Maps-->
    <div class="mapEstablecimiento col-xs-12 col-md-12" id="street-view">  
    </div>
<!--Contact-->
    <div class="contactEstablecimiento col-xs-12 col-md-12"> 
       <div class="contact col-xs-3 col-md-3"> 
           <label>Contacto</label>
       </div>
    <!--<<div class="email col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-envelope"></span>
            <email>contacto@contacto.com</email>
        </div>-->
        <div class="telephone col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-earphone"></span>
            <?php echo $data['Tel'] ; ?>
        </div>
        <div class="facebook col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-thumbs-up"></span>
            <a href="https://www.facebook.com">Facebook</a>
        </div>
        <div class="twitter col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-heart"></span>
            <a href="https://www.twitter.com">Twitter</a>
        </div>       
    </div>
</div> 
</div>
</div>

<!--Reviews-->
<div class="scrReviews col-xs-12 col-md-12">
   <div class="contReviews col-xs-12 col-md-12">
       <div class="reviews col-xs-11 col-md-11">
       <div class="headerReviews"><h1>Reviews</h1></div> 
        <div class="textReviews col-xs-10 col-md-10">
            <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac arcu id mi pulvinar ullamcorper. Sed eros arcu, laoreet fringilla magna et, molestie tristique ipsum. Vivamus imperdiet libero ac mauris vestibulum, tempus porttitor nisi volutpat. Donec at dui nunc. Etiam dignissim leo leo, quis aliquet erat consectetur at. Proin mauris felis, rutrum et finibus a, lobortis vel orci. Curabitur vitae risus ac augue varius vulputate vitae in metus. Curabitur eget tortor porttitor, varius urna ut, efficitur tortor.
        </p>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac arcu id mi pulvinar ullamcorper. Sed eros arcu, laoreet fringilla magna et, molestie tristique ipsum. Vivamus imperdiet libero ac mauris vestibulum, tempus porttitor nisi volutpat. Donec at dui nunc. Etiam dignissim leo leo, quis aliquet erat consectetur at. Proin mauris felis, rutrum et finibus a, lobortis vel orci. Curabitur vitae risus ac augue varius vulputate vitae in metus. Curabitur eget tortor porttitor, varius urna ut, efficitur tortor.
        </p>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac arcu id mi pulvinar ullamcorper. Sed eros arcu, laoreet fringilla magna et, molestie tristique ipsum. Vivamus imperdiet libero ac mauris vestibulum, tempus porttitor nisi volutpat. Donec at dui nunc. Etiam dignissim leo leo, quis aliquet erat consectetur at. Proin mauris felis, rutrum et finibus a, lobortis vel orci. Curabitur vitae risus ac augue varius vulputate vitae in metus. Curabitur eget tortor porttitor, varius urna ut, efficitur tortor.
        </p>
        <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac arcu id mi pulvinar ullamcorper. Sed eros arcu, laoreet fringilla magna et, molestie tristique ipsum. Vivamus imperdiet libero ac mauris vestibulum, tempus porttitor nisi volutpat. Donec at dui nunc. Etiam dignissim leo leo, quis aliquet erat consectetur at. Proin mauris felis, rutrum et finibus a, lobortis vel orci. Curabitur vitae risus ac augue varius vulputate vitae in metus. Curabitur eget tortor porttitor, varius urna ut, efficitur tortor.
        </p>
        </div>
       </div>
   </div>
</div>


<script>

var panorama;
var index=cor.indexOf(",");
var x=cor.substring(0,index);
var y=cor.substring(index+1,cor.legth);
var nx=parseFloat(x);
var ny=parseFloat(y);
// StreetViewPanoramaData of a panorama just outside the Google Sydney office.
var outsideGoogle;

// StreetViewPanoramaData for a custom panorama: the Google Sydney reception.
function getReceptionPanoramaData() {
  return {
    location: {
      pano: 'reception',  // The ID for this custom panorama.
      description: 'Google Sydney - Reception',
      latLng: new google.maps.LatLng(nx, ny)
    },
    links: [{
      heading: 195,
      description: 'Exit',
      pano: outsideGoogle.location.pano
    }],
    copyright: 'Imagery (c) 2010 Google',
    tiles: {
      tileSize: new google.maps.Size(1024, 512),
      worldSize: new google.maps.Size(2048, 1024),
      centerHeading: 105,
      getTileUrl: function(pano, zoom, tileX, tileY) {
        return 'images/' +
            'panoReception1024-' + zoom + '-' + tileX + '-' + tileY + '.jpg';
      }
    }
  };
}

function initPanorama() {
  panorama = new google.maps.StreetViewPanorama(
      document.getElementById('street-view'),
      {
        pano: outsideGoogle.location.pano,
        // Register a provider for our custom panorama.
        panoProvider: function(pano) {
          if (pano === 'reception') {
            return getReceptionPanoramaData();
          }
        }
      });

  // Add a link to our custom panorama from outside the Google Sydney office.
  panorama.addListener('links_changed', function() {
    if (panorama.getPano() === outsideGoogle.location.pano) {
      panorama.getLinks().push({
        description: 'Google Sydney',
        heading: 25,
        pano: 'reception'
      });
    }
  });
}

function initialize() {
  // Use the Street View service to find a pano ID on Pirrama Rd, outside the
  // Google office.
  var streetviewService = new google.maps.StreetViewService;
  streetviewService.getPanorama(
      {location: {lat: nx, lng: ny}},
      function(result, status) {
        if (status === google.maps.StreetViewStatus.OK) {
          outsideGoogle = result;
          initPanorama();
        }
      });
}

    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWD3SIg_Ya0n_0tTsCt1Dm-vMnOTAqyWU&signed_in=true&callback=initialize">
    </script>
</body>
</html>