<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Dircun</title>
    <link rel="stylesheet" href="">
    <!-- Hojas de estilos -->
    <link rel="stylesheet" href="css/styleAddPlace.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--<link rel="stylesheet" href="css/generics.css">-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
    <link href="animate.css" rel="stylesheet">
</head>
<body>
<div class="screen">
    <div class="container">
        <!--Container-->
<div class="contentAddPlace col-xs-11 col-md-8"> 
<!--Header-->
    <div class="headerAddPlace"><h1>Agregar establecimiento</h1></div> 
<!--Data--> 
<div class="infogral col-xs-6 col-md-6">
    <label for="">Información general</label>
        <div class="form-group">
            <input type="text" class="form-control" id="Name" placeholder="Nombre">  	
        </div>
               
        <div class="form-group">
            <input type="text" class="form-control" id="Address" placeholder="Dirección">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" id="Type" placeholder="Tipo">
        </div>

        <div class="form-group">
            <input type="tel" class="form-control" id="Telephone" placeholder="Núm. telefónico">
        </div>
  	    	                
        <div class="form-group">
            <input type="url" class="form-control" id="Website" placeholder="Website">
        </div>
        
        <div class="form-group">
            <input type="url" class="form-control" id="Facebook" placeholder="Facebook">
        </div>
        
        <div class="form-group">
            <input type="url" class="form-control" id="Twitter" placeholder="Twitter">
        </div>
        

       
        <label for="">Decripción</label>
        <textarea class="form-control" rows="6" placeholder="¿Qué hace diferente a tu establecimiento?... Escríbelo en menos de 6 líneas."></textarea>
        
</div>
<div class="horario col-xs-6 col-md-6">
    <label for="">Apertura</label>
        <div class="form-group">
            <input type="time" class="form-control" id="Open" placeholder="Abierto">
        </div>
        
        <label for="">Cierre</label>
        <div class="form-group">
            <input type="time" class="form-control" id="Closed" placeholder="Cerrado">
        </div>
     
        <br>
        <button type="submit" class="btn btn-default" id="Enviar">Add place</button>
</div>
    
<!--Maps-->
    <div class="mapAddPlace col-xs-6 col-md-6"> 
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.2811992401503!2d-86.9087486854458!3d21.180985485916107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4c2c0162fb4071%3A0xc881c60dfcc845c!2sUniversidad+Polit%C3%A9cnica+de+Quintana+Roo!5e0!3m2!1ses-419!2smx!4v1479573890259" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
    </div>
</div>

</body>
</html>