<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Directorio</title>
    <meta name="author" content="Reef Studios">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">   
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
    <!-- Hojas de estilos -->            
    <link rel="stylesheet" href="css/styleBusquedas.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">
</head>
<body>
    <div class="searchScreen">
        <div class="logo">
            <a href="index.php"><img src="img/DirCun.gif" alt="" class="img-responsive"></a>
        </div>
        <div class="serchContent">
            <div class="searchHead">
                <h2>Resultados para: <span> Educación</span></h2>
                <hr>
            </div>
            <div class="searchContainer">
                <div class="element wow fadeInUp" data-wow-delay="0.2s">
                    <div class="image">
                        <img src="img/Universidad.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Universidad Politécnica de Quintana Roo</h2>
                        <p>¡En tiempos de cambios seamos pro activos. Somos una Universidad con desarrollo inteligente, sostenible e integrador; efectiva y eficiente. Los caminos hay que construirlos, los horizontes hay que alcanzarlos y el futuro hay que soñarlo. Integrate al progreso continuo, las cosas no ocurren por si mismas, ¡Hagamos que sucedan!.
                        <br><a href="UniversidadPolitecnicaDeQuintanaRoo.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.4s">
                    <div class="image">
                        <img src="img/UniCaribe.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Universidad del Caribe</h2>
                        <p>Impartir educación superior con validez oficial para formar integralmente profesionales competentes con un amplio sentido ético, humanístico y nacionalista, con un elevado compromiso social, y aptos para generar y aplicar creativamente conocimientos en la solución de problemas;
                        <br> <a href="UniversidadDelCaribe.php" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.6s">
                    <div class="image">
                        <img src="img/UniAnahuac.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Universidad Anahuac Cancun</h2>
                        <p>La Universidad Anáhuac Cancún es una institución de educación superior que, como comunidad de profesores y estudiantes, busca en todo la verdad y el bien, y se empeña en la formación integral de personas que ejerzan su liderazgo y así contribuyan a la transformación cristiana de la sociedad y de la cultura.
                        <br><a href="UniversidadAnahuacCancun.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.8s">
                    <div class="image">
                        <img src="http://www.enlinealasalle.com/file.php/1/la_salle.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Universidad La Salle Cancun</h2>
                        <p>La Salle Cancún como agente de cambio social es reconocida por la calidad de sus egresados, competentes, innovadores, creativos, con valores y con un amplio espíritu de servicio que impactan de forma positiva a la sociedad en el marco del modelo educativo lasallista. La Universidad se caracteriza por la calidad de sus colaboradores y la buena relación pedagógica y personal entre alumno y maestro.
                        <br><a href="UniversidadLaSalleCancun.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="endSearch">
                    <br>
                    <h2>- Fin de las busquedas -</h2>
                    <br>
                </div>                
            </div>            
        </div>        
    </div>
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="dist/wow.js"></script>
    <script>
        new WOW().init();
    </script> 
</body>
</html>