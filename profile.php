<?php
    require 'funtions/conexion.php';
    session_start();
    if(isset($_SESSION['id']))
    {
        $id = $_SESSION['id'];
        $query = $connection -> prepare(
	'SELECT establecimientos.idEs, establecimientos.Nombre, usuarios.nomUs, usuarios.logo, establecimientos.img, establecimientos.Direccion FROM usuarios inner join establecimientos WHERE establecimientos.idEm=usuarios.idUs AND establecimientos.idEm=:id');

        $query->bindValue(':id', intval($id), PDO::PARAM_INT);


	$query -> execute(array(
	'id' => $id));
	
	$data = $query -> fetchAll();

    }
    else
    {
        header('Location: ../index.php');
        die;
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<link rel="stylesheet" href="css/styleBienvenida.css">
</head>
<body>
  <?php 
    if(empty($data)){
        header('location: addPlacen.php?id='.$id);
    }
    else{
        echo '<img class="logoimg" src="'.$data[0]['logo'].'"/>';
        foreach($data as $ndata)
        {
        echo '<img class="imag" src="'.$ndata['img'].'"/>';
        #echo '<div class="empresa">'.$ndata['nomUs'].'</div>';
        echo '<a href="#"> <div class="estab">'.$ndata['Nombre'].'</div> </a>';
        echo '<a href="pruebas.php?id='.$ndata['idEs'].'"> <div class="estab">'.$ndata['Direccion'].'</div> </a>';
        }
    }
    echo '<a href="addPlacen.php?id='.$id.'">Agregar otro establecimiento</a>';
  ?>
  <a href="actions/bye.php">Cerrar Sesión</a>
</body>
</html>