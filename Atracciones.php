<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Buscar: Atracciones - Dircun</title>
    <meta name="author" content="Reef Studios">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">   
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
    <!-- Hojas de estilos -->            
    <link rel="stylesheet" href="css/styleBusquedas.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">
</head>
<body>
    <div class="searchScreen">
        <div class="logo">
            <a href="index.php"><img src="img/DirCun.gif" alt="" class="img-responsive"></a>
        </div>
        <div class="serchContent">
            <div class="searchHead">
                <h2>Resultados para: <span> Atracciones</span></h2>
                <hr>
            </div>
            <div class="searchContainer">
                <div class="element wow fadeInUp" data-wow-delay="0.2s">
                    <div class="image">
                        <img src="img/Palapas.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Parque de las Palapas</h2>
                        <p>El Parque de las Palapas es un gran parque en la zona continental de la ciudad de Cancún, que sus habitantes utilizan como punto de reunión.
                        Alrededor del parque se encuentran puestos de venta de aguas (jugos, zumos), artesanías y espectáculos callejeros como mimos o grupos musicales.
                        <br><a href="ParquePalapas.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.2s">
                    <div class="image">
                        <img src="img/PlayaLangosta.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Playa Langosta</h2>
                        <p>Playa Langosta es una de las playas más populares y familiares de Cancún. Debido a sus aguas calmas es una de las mejores playas para el nado y para visitar con niños pequeños. Por otra parte, al no tener grandes olas, no es una playa apta para el surf, pero sí para los deportes de playa como el fútbol y voleibol playeros.
                        <br><a href="PlayaLangosta.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>                   
                <div class="endSearch">
                    <br>
                    <h2>- Fin de las busquedas -</h2>
                    <br>
                </div>                
            </div>            
        </div>        
    </div>
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="dist/wow.js"></script>
    <script>
        new WOW().init();
    </script> 
</body>
</html>