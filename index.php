<?php
if(isset($_POST['cat'])){
    $cat = $_POST['cat'];
    switch ($cat) {
        case '1':
            header('Location: Educacion.php');
            break;
        case '2':
            header('Locattion: Restaurantes.php');
            break;
        case '3':
            header('Location: Centros.php');
            break;
        case '4':
            header('Location: Compras.php');
            break;
        case '5':
            header('Location: Hoteles.php');
            break;
        case '6':
            header('Location: Atracciones.php');
            break;
        default:
            header('Location: Educacion.php');
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Metadatos -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dircun</title>
        <meta name="author" content="Reef Studios">
        <meta name="description" content="Dircun directorio virtual de las mejores empresas de Cancún">
        <meta name="keywords" content="dircun, cancun, empresas, atracciones, directorio, agencia">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="icon.ico">   
        <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
        <!-- Hojas de estilos -->        
        <link rel="stylesheet" href="css/cards.css">
        <link rel="stylesheet" href="css/set1.css"> 
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">        
        <link rel="stylesheet" href="fonts/flaticon.css">                
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">
        <link rel="stylesheet" href="css/animate.css">    
    <!-- Fin Metadatos -->
</head>
<body>
    <!-- Navegadores obsoletos -->
        <!--[if lt IE 8]>
            <p class="browserupgrade">Estás usando una versión <strong>obsoleta</strong> del navegador. Porfavor <a href="http://browsehappy.com/">Actualiza tu navegador para poder disfrutar de todo el contenido del directorio.</p>
        <![endif]-->
    <!-- Fin Navegadores obsoletos -->
    
    <!-- Main Screen 100% - 200% -->
    <div class="main">
        <header>
            <nav>
                <ul>
                    <div class="logo">
                    </div>
                    <div class="navigation">
                       <div class="container">
                           <a href="login.php"><p class="wow fadeInDown" data-wow-delay="0.1s">Acceder <i class="fa fa-address-book-o" aria-hidden="true"></i></p></a>
                       </div>                        
                    </div>
                </ul>
            </nav>
        </header>
        <div class="header wow fadeInUp" data-wow-delay="0.1s">
            <div class="weather">
               <div class="weatherIcon">
                   <i class="flaticon-cloud"></i>
                   <p>29 °C</p>
               </div>
               <div class="title">
                   <img src="img/DIRCUNblanco5px.png" alt="" class="img-responsive">
                   <h2>Las mejores empresas y sitios para ti</h2>
               </div>
               <div class="search">
                   <form action="" method="post">
                          <select name="cat">
                              <option value="Educacion">Selecciona: </option>
                              <?php  
                                require 'funtions/conexion.php';
                                $query = $connection -> prepare('SELECT * from categorias');
                                $query -> execute();
                                $data = $query -> fetchAll();                              
                                foreach ($data as $ndata) {
                                echo '<option value="'.$ndata['idCat'].'">'.$ndata['categ'].'</option>';
                                 }
                            ?>
                          </select>
                       <input type="submit" value="Buscar">
                   </form>
               </div>
            </div>
        </div>
    </div>
    <!-- Second Screen 200% - 300% -->
    <div class="allCategories">
        <div class="allCategoriesHead">
            <h2>Explora todo Cancún</h2>
        </div>
        <div class="content">
            <div class="grid">
                <figure class="effect-sadie">
				    <img src="img/Universidad.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Educación</span></h2>
				        <p>Explora todas las opciones educativas.</p>
				        <a href="Educacion.php">View more</a>
				    </figcaption>			
				</figure>
               <figure class="effect-sadie">
				    <img src="img/2.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Restaurantes</span></h2>
				        <p>Dale el gusto a tu paladar.</p>
				        <a href="Restaurantes.php">View more</a>
				    </figcaption>			
				</figure>
               <figure class="effect-sadie">
				    <img src="img/3.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Centros Comerciales</span></h2>
				        <p>Explora, compra y diviertete.</p>
				        <a href="Centros.php">View more</a>
				    </figcaption>
				</figure>
            </div>
        </div>
        <div class="content">
            <div class="grid">
                <figure class="effect-sadie">
				    <img src="img/Compras.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Compras</span></h2>
				        <p>Conoce y dejate consentir.</p>
				        <a href="Compras.php">View more</a>
				    </figcaption>			
				</figure>
               <figure class="effect-sadie">
				    <img src="img/Hoteles.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Hoteles</span></h2>
				        <p>Hospedate en los mejores Hoteles del Caribe.</p>
				        <a href="Hoteles.php">View more</a>
				    </figcaption>			
				</figure>
               <figure class="effect-sadie">
				    <img src="img/Palapas.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Atracciones</span></h2>
				        <p>Adentrate en la cuidad.</p>
				        <a href="Atracciones.php">View more</a>
				    </figcaption>			
				</figure>
            </div>
        </div>
    </div>       
    <!-- Third Screen 300% - 400% -->
    <div class="AdsScreen">
        <div class="promotion wow fadeInLeft" data-wow-delay="0.2s">
            <h2>¡Promoción!</h2>
        </div>
        <div class="promotionTitle wow fadeInLeft" data-wow-delay="0.4s">
            <h2>Hard Rock Hotel</h2>
        </div>
        <div class="promotionDate wow fadeInLeft" data-wow-delay="0.6s">
            <h3>01/12/16 - Deluxe Gold - $5,886 MXN</h3>            
        </div>
        <div class="promotionJoin wow fadeInLeft" data-wow-delay="0.8s">
            <button><a href="">Ver detalles</a></button>
        </div>        
    </div>
    <!-- Fourth Screen 400% - 500% -->
   <div class="secundaryAds">
        <div class="secundaryHead">
            <h2>Tenemos lo que estás buscando</h2>
            <h3>Lo más buscado</h3>
        </div>
        <div class="Ads">
                <div class="containerCard">
                <div class="column">
                    <div class="post-module wow fadeInUp" data-wow-delay="0.2s">
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day">27</div>
                                <div class="month">Mar</div>
                            </div>
                            <img src="img/FiestaAmericana.jpg"/>
                        </div>
                        <div class="post-content">
                            <div class="category">-35%</div>
                            <h1 class="title">Grand Fiesta Americana</h1>
                            <h2 class="sub_title">Coral Beach Cancun</h2>
                            <p class="description">Habitación Gran Club, vistas al océano
                                Suite, 1 cama King size (Master)
                            </p>
                            <div class="post-meta"><span class="timestamp"><i class="fa fa-clock-">o</i> 3 días restantes</span><span class="comments"><i class="fa fa-comments"></i><a href="#"> 5 comentarios</a></span></div>
                        </div>
                    </div>
                </div>
            </div>
             
             <div class="containerCard">
                <div class="column">
                    <div class="post-module wow fadeInUp" data-wow-delay="0.4s">
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day">27</div>
                                <div class="month">Mar</div>
                            </div>
                            <img src="img/JugoDeLimon.jpg"/>
                        </div>
                        <div class="post-content">
                            <div class="category">-12%</div>
                            <h1 class="title">Jugo De Limón</h1>
                            <h2 class="sub_title">Restaurant único</h2>
                            <p class="description">Una cevichería de estilo mexicano con la mejor y más popular cocina de la costa mexicana, además de otros platos tradicionales.</p>
                            <div class="post-meta"><span class="timestamp"><i class="fa fa-clock-">o</i> 2 días</span><span class="comments"><i class="fa fa-comments"></i><a href="#"> 2 comentarios</a></span></div>
                        </div>
                    </div>
                </div>
            </div>
              
            <div class="containerCard">
                <div class="column">
                    <div class="post-module wow fadeInUp" data-wow-delay="0.6s">
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day">27</div>
                                <div class="month">Mar</div>
                            </div>
                            <img src="img/Mandala.jpg"/>
                        </div>
                        <div class="post-content">
                            <div class="category">6 MSI</div>
                            <h1 class="title">Mandala Beach Club</h1>
                            <h2 class="sub_title">Despierta tus sentidos</h2>
                            <p class="description">Las mejores fiestas en la playa ocurren en un solo lugar: Mandala Beach Club, el Mar Caribe, bikinis, bebidas tropicales.</p>
                            <div class="post-meta"><span class="timestamp"><i class="fa fa-clock-">o</i> 7 días</span><span class="comments"><i class="fa fa-comments"></i><a href="#"> 10 comentarios</a></span></div>
                        </div>
                    </div>
                </div>
            </div>            
            </div>
        </div>
        
    <!-- Fifth Screen 500% - 600% -->
    <div class="monthScreen">
        <div class="monthHead">
            <h2>Recomendación de la semana</h2>                       
        </div>
        <div class="monthContent">
            <div class="fx-card wow fadeInUp" data-wow-delay="0.2s">
                <div class="fx-card__img">
                    <img src="img/Interceramic.jpg" alt=""/>
                </div>
                <div class="fx-card__info">
                    <h1 class="fx-card__title">Interceramic Sucursal Portillo</h1>
                    <p class="fx-card__text">Dale a tu hogar el lugar que merece.
                    En pisos y azulejos Interceramic tiene todo para remodelar o ampliar tu hogar.
                    Interceramic, Simplemente lo mejor.</p>
                    <p>Después de más de 35 años en el mercado, Interceramic se ha convertido en líder indiscutible, no solamente como fabricante, sino también como distribuidor de pisos y azulejos cerámicos, muebles de baño, materiales para instalación y piedra natural.</p>
                    <a href="">Más información</a>
                </div>
            </div>
            <div class="fx-card wow fadeInUp" data-wow-delay="0.4s">
                <div class="fx-card__img">
                    <img src="img/Universidad.jpg" alt=""/>
                </div>
                <div class="fx-card__info">
                    <h1 class="fx-card__title">Universidad Politécnica de Quintana Roo</h1>
                    <p class="fx-card__text">¡En tiempos de cambios seamos pro activos. Somos una Universidad con desarrollo inteligente, sostenible e integrador; efectiva y eficiente. Los caminos hay que construirlos, los horizontes hay que alcanzarlos y el futuro hay que soñarlo. Integrate al progreso continuo, las cosas no ocurren por si mismas, ¡Hagamos que sucedan!.</p>
                    <a href="">Más información</a>                    
                </div>
            </div>
        </div>
    </div>
    <!-- Sixth Screen Newslatter -->
    <div class="newslatter">
       <div class="newslatterHead">
           <h3>Sucribete</h3>
           <h4>Recibe las mejores ofertas semanales</h4>
       </div>
        <input type="text" placeholder="Correo: " id="Correo">
        <input type="button" value="Suscribirse" id="Suscribirse">
    </div>
    
    <footer> 
        <div class="container">
            <div class="col-md-6 info wow fadeInUp" data-wow-delay="0.1s">
                <img src="img/DIRCUNblanco.png" alt="" class="img-responsive">
                <p>Dircun, la plataforma que entiende lo que quieres ver y tener, conoce, visita, consume, apoya, opina y participa.</p>
            </div>
            <div class="col-md-6 text-center social wow fadeInUp" data-wow-delay="0.3s">
                <h2>Get in touch!</h2>
                <p><i class="fa fa-facebook fa-2x wow fadeInUp" data-wow-delay="0.3s" aria-hidden="true"></i>
                <i class="fa fa-twitter fa-2x wow fadeInUp" data-wow-delay="0.3s" aria-hidden="true"></i>
                <i class="fa fa-linkedin fa-2x wow fadeInUp" data-wow-delay="0.2s" aria-hidden="true"></i>                
            </div>
        </div>
    </footer>
    <!-- Footer -->
    <div class="footerCopy">
        <div class="container">
           <div class="copy col-md-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
               <p>Dircun © 2016 / All Rights Reserved</p> 
           </div>
           <div class="nav col-md-6 col-xs-12 wow fadeInUp" data-wow-delay="0.3s">
               <p>Inicio / Acerca de / Miembros</p>
           </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-2.2.4.min.js"><\/script>')</script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script> 
    <script src="dist/wow.js"></script>
    <script>
        new WOW().init();
    </script>   
</body>
</html>