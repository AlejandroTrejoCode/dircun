<?php 
require 'funtions/conexion.php';
$idEm=$_GET['id'];
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>¡Bienvenido!</title>
    <link rel="stylesheet" href="">
    <!-- Hojas de estilos -->
    <link rel="stylesheet" href="css/styleAddPlace.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--<link rel="stylesheet" href="css/generics.css">-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
    <link href="animate.css" rel="stylesheet">
</head>
<body>
<div class="screen">
    <div class="container">
        <!--Container-->
<form method="post" action="actions/addestab.php">
<div class="contentAddPlace col-xs-11 col-md-8"> 
<!--Header-->
    <div class="headerAddPlace"><h5>Agregar establecimiento</h5></div> 
<!--Data--> 
<div class="infogral col-xs-6 col-md-6">
    <label for="">Información general</label>
    <?php echo '<input type="hidden" name="idEm" value="'.$idEm.'">' ?>
        <div class="form-group">
            <input type="text" class="form-control" id="Name" placeholder="Nombre" name="nom">  	
        </div>
               
        <div class="form-group">
            <input type="text" class="form-control" id="Address" placeholder="Dirección" name="dir">
        </div>
        
        <div class="form-group">
            <input type="hidden" id="px" name="x">
        </div>
        <div class="form-group">
            <input type="hidden" id="py" name="y">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="Cords" placeholder="Coordenadas" name="coords">
        </div>

        <div class="form-group">
            <select name="cat">
                <option value="">Selecciona:</option>
                <?php  
                    $query = $connection -> prepare('SELECT * from categorias');


                    $query -> execute();
  
                    $data = $query -> fetchAll();
                    foreach ($data as $ndata) {
                        echo '<option value="'.$ndata['idCat'].'">'.$ndata['categ'].'</option>';
                    }
                    
                ?>
            </select>
        </div>

        <div class="form-group">
            <input type="tel" class="form-control" id="Telephone" placeholder="Núm. telefónico" name="tel">
        </div>

        <div class="form-group">
            <input type="email" class="form-control" id="Email" placeholder="Correop de contacto" name="email">
        </div>
  	    	                
        <div class="form-group">
            <input type="text" class="form-control" id="Website" placeholder="Website" name="webS">
        </div>
        
        <div class="form-group">
            <input type="text" class="form-control" id="Facebook" placeholder="Facebook" name="social">
        </div>
        
        <label for="">Decripción</label>
        <textarea class="form-control" rows="6" placeholder="¿Qué hace diferente a tu establecimiento?... Escríbelo en menos de 10 líneas." name="descr"></textarea>
        
</div>
<div class="horario col-xs-6 col-md-6">
    <label for="">Horario</label>
        <div class="form-group">
            <input type="text" class="form-control" id="Open" placeholder="Abierto" name="hor">
        </div>
        <label for="">Imagen</label>
        <div class="form-group">
            <input type="file" name="img">
        </div>
     
        <br>
        <button type="submit" class="btn btn-default" id="Enviar">Add place</button>
</div>
    
<!--Maps-->
<div class="mapAddPlace col-xs-6 col-md-6" id="map"> </div>
</form>
<script>
var map;
var markers = [];

	function initMap() {
	// Create a map object and specify the DOM element for display. 21.140125, -86.882550
	var map = new google.maps.Map(document.getElementById('map'), {
	center: {lat: 21.140125, lng: -86.882550},
	scrollwheel: true,
	zoom: 10
	});
			       	 
	google.maps.event.addListener(map, "click", function(event) {
	deleteMarkers();
	placeMarkerAndPanTo(event.latLng, map);
	var lat = event.latLng.lat();
	var lng = event.latLng.lng();
		document.getElementById("px").value = lng;
		document.getElementById("py").value = lat;
		var cor=lng+","+lat;
		document.getElementById("Cords").value=cor;
	});

			        } 
	function placeMarkerAndPanTo(latLng, map) {
	var marker = new google.maps.Marker({
	position: latLng,
	map: map
	});
	// map.panTo(latLng);
	markers.push(marker);
	}
	function setMapOnAll(map) {
		for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
		}
	}
	function clearMarkers() {
		setMapOnAll(null);
	}

		// Shows any markers currently in the array.
	function showMarkers() {
		setMapOnAll(map);
	}

		// Deletes all markers in the array by removing references to them.
	function deleteMarkers() {
		clearMarkers();
		markers = [];
	}
</script> 
     <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBWD3SIg_Ya0n_0tTsCt1Dm-vMnOTAqyWU&signed_in=true&callback=initMap">
    </script>
</div>
    </div>
</div>

</body>
</html>