<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Sign Up</title>
        <meta name="author" content="Reef Studios">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">        
        <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
        <!-- Hojas de estilos -->
        <link rel="stylesheet" href="css/styleSignUp.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <!--<link rel="stylesheet" href="css/generics.css">-->
        <link rel="stylesheet" href="fonts/flaticon.css">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,700,900" rel="stylesheet">
        <link href="animate.css" rel="stylesheet">
</head>
<body>

	<div class="loginContainer">
		<div class="loginHeader">
			<h3 class="loginHeaderText" style="margin: 0;">SIGN UP</h3>
		</div>
		<form class="loginData" enctype="multipart/form-data" method="post" action="actions/upload.php">
      <br><br><br> <!--AQUÍ HAY QUE METER UN PADDING O UN MARGIN-->
      <div class="form-group">
        <input type="text" class="form-control" id="InputName1" placeholder="Name" name="nom">
      </div>
			<div class="form-group">

   			<input type="email" class="form-control" id="InputEmail1" placeholder="Email" name="email">
  			</div>
  		<div class="form-group">
        <input type="password" class="form-control" id="InputPassword1" placeholder="Password" name="pass">
        </div>
      <div class="form-group">

        <input type="password" class="form-control" id="InputPassword1" placeholder="Repeat password" name="pass2">
      </div>
  		<div class="form-group">

        <input type="tel" class="form-control" id="InputTel1" placeholder="Telephone number" name="tel">
      </div>
      <div class="form-group">
        <input type="Text" class="form-control" id="InputCountry" placeholder="Codigo postal" name="cp">
      </div>
      <div class="form-group">
        <input type="Text" class="form-control" id="InputCountry" placeholder="Descripcion" name="descr">
      </div>
      <div class="form-group">
        <input type="Text" class="form-control" id="InputCountry" placeholder="Razon social S.A de C.v" name="razon">
      </div>
      <div class="form-group">
        <input type="file" class="btn btn-default" value="Añadir logo" name="uploaded"></input>
      </div>
  		<button type="submit" class="btn btn-default">Create account</button>
		</form>
	</div>
</body>
</html>