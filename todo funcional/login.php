<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Log in</title>
        <meta name="author" content="Reef Studios">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">        
        <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
        <!-- Hojas de estilos -->
        <link rel="stylesheet" href="css/styleLogin.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <!--<link rel="stylesheet" href="css/generics.css">-->
        <link rel="stylesheet" href="fonts/flaticon.css">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,700,900" rel="stylesheet">
        <link href="animate.css" rel="stylesheet">
</head>
<body>

	<div class="loginContainer">
		<div class="loginHeader">
			<h3 class="loginHeaderText" style="margin: 0;">LOG IN</h3>
		</div>
		<form class="loginData" method="post" action="actions/login.php">
			<br><br><br> <!--AQUÍ HAY QUE METER UN PADDING O UN MARGIN-->
			<div class="form-group">
   				<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" name="user">
  			</div>
  			<div class="form-group">
   				<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="pass">
  			</div>
  			<br>
  			<button type="submit" class="btn btn-default">Join</button>
  			<h5 class="redirSignUp">¿No tienes cuenta?<a href="singup.php"> Regístrate</a></h5>
			
		</form>
	</div>
</body>
</html>