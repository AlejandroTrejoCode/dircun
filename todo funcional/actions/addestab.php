<?php 
require '../funtions/conexion.php';
if (isset($_POST['cat'])) {
	$idEm=$_POST['idEm'];
	$nom=$_POST['nom'];
	$dir=$_POST['dir'];
	$coords=$_POST['coords'];
	$tel=$_POST['tel'];
	$cat=$_POST['cat'];
	$email=$_POST['email'];
	$WebS=$_POST['webS'];
	$hor=$_POST['hor'];
	$social=$_POST['social'];
	$descr=$_POST['descr'];
	if (isset($_FILES['uploaded'])) {
		$uploaded=$_FILES['uploaded'];
		$extencion=pathinfo($uploadedFile['name'],PATHINFO_EXTENSION);
		if ($extencion!="png" or $extencion!="jpg") {
		move_uploaded_file($uploadedFile['tmp_name'], '../img/estab/'.$nom.'.'.$extencion);	
		$img='img/estab/'.$nom.'.'.$extencion;
		$query = $connection->prepare(
        	'INSERT INTO establecimientos VALUES (null, :idEm, :nom, :dir, :coords,:tel,:cat,:email,:webS,:hor,:descr,:img)'
    		);

    		$query -> execute(array(
        	'idEm'=>$idEm, 'nom'=>$nom, 'dir'=>$dir,'coords'=>$coords,'tel'=>$tel,'cat'=>$cat,,'email'=>$email,'webS'=>$webS,'hor'=>$hor,'social'=>$social,'descr'=>$descr,'img'=>$img
    		));

    		header('Location: ../estab.php');
		} else {
			header('Location: ../singup.php');
		}			
		} 
	else
	{
		$img="img/estab/null.png";
		$query = $connection->prepare(
        	'INSERT INTO establecimientos VALUES (null, :idEm, :nom, :dir, :coords,:tel,:cat,:email,:webS,:hor,:descr,:img)'
    		);

    		$query -> execute(array(
        	'idEm'=>$idEm, 'nom'=>$nom, 'dir'=>$dir,'coords'=>$coords,'tel'=>$tel,'cat'=>$cat,,'email'=>$email,'webS'=>$webS,'hor'=>$hor,'social'=>$social,'descr'=>$descr,'img'=>$img
    		));

    		header('Location: ../estab.php');
	}
} else {
	header('Location: ../singup.php');
}
?>