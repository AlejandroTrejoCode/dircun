<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Metadatos -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Directorio</title>
        <meta name="author" content="Reef Studios">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">   
        <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
        <!-- Hojas de estilos -->        
        <link rel="stylesheet" href="css/cards.css">
        <link rel="stylesheet" href="css/set1.css"> 
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">        
        <link rel="stylesheet" href="fonts/flaticon.css">        
        <link href="animate.css" rel="stylesheet">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">    
    <!-- Fin Metadatos -->
</head>
<body>
    <!-- Navegadores obsoletos -->
        <!--[if lt IE 8]>
            <p class="browserupgrade">Estás usando una versión <strong>obsoleta</strong> del navegador. Porfavor <a href="http://browsehappy.com/">Actualiza tu navegador para poder disfrutar de todo el contenido del directorio.</p>
        <![endif]-->
    <!-- Fin Navegadores obsoletos -->
    
    <!-- Main Screen 100% - 200% -->
    <div class="main">
        <header>
            <nav>
                <ul>
                    <div class="logo">
                        
                    </div>
                    <div class="navigation">
                       <div class="container">
                           <a href="login.php"><p>Acceder <i class="fa fa-address-book-o" aria-hidden="true"></i></p></a>
                       </div>                        
                    </div>
                </ul>
            </nav>
        </header>
        <div class="header">
            <div class="weather">
               <div class="weatherIcon">
                   <i class="flaticon-cloud"></i>
                   <p>29 °C</p>
               </div>
               <div class="title">
                   <img src="img/DIRCUNblanco5px.png" alt="" class="img-responsive">
                   <h2>Las mejores empresas y sitios para ti</h2>
               </div>
               <div class="search">
                   <form action="" method="post" action="www.google.com">
                          <select name="cat">
                            <option value="">Selecciona:</option>
                            <?php  
                                require 'funtions/conexion.php';

                                $query = $connection -> prepare('SELECT * from categorias');


                                $query -> execute();
  
                                $data = $query -> fetchAll();
                    
                                foreach ($data as $ndata) {
                                echo '<option value="'.$ndata['idCat'].'">'.$ndata['categ'].'</option>';
                                 }
                            ?>
                            </select>
                       <input type="submit" value="Buscar">
                   </form>
               </div>
            </div>
        </div>
    </div>
    <!-- Second Screen 200% - 300% -->
    <div class="allCategories">
        <div class="allCategoriesHead">
            <h2>Explora todo Cancún</h2>
        </div>
        <div class="content">
            <div class="grid">
                <figure class="effect-sadie">
				    <img src="img/Universidad.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Educación</span></h2>
				        <p>Explora todas las opciones educativas.</p>
				        <a href="#">View more</a>
				    </figcaption>			
				</figure>
               <figure class="effect-sadie">
				    <img src="img/2.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Restaurantes</span></h2>
				        <p>Dale el gusto a tu paladar.</p>
				        <a href="#">View more</a>
				    </figcaption>			
				</figure>
               <figure class="effect-sadie">
				    <img src="img/3.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Centros Comerciales</span></h2>
				        <p>Explora, compra y diviertete.</p>
				        <a href="#">View more</a>
				    </figcaption>			
				</figure>
            </div>
        </div>
        <div class="content">
            <div class="grid">
                <figure class="effect-sadie">
				    <img src="img/Compras.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Compras</span></h2>
				        <p>Conoce y dejate consentir.</p>
				        <a href="#">View more</a>
				    </figcaption>			
				</figure>
               <figure class="effect-sadie">
				    <img src="img/Hoteles.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Hoteles</span></h2>
				        <p>Hospedate en los mejores Hoteles del Caribe.</p>
				        <a href="#">View more</a>
				    </figcaption>			
				</figure>
               <figure class="effect-sadie">
				    <img src="img/Palapas.jpg" alt="img02"/>
				    <figcaption>
				        <h2> <span>Atracciones</span></h2>
				        <p>Adentrate en la cuidad.</p>
				        <a href="#">View more</a>
				    </figcaption>			
				</figure>
            </div>
        </div>
    </div>       
    <!-- Third Screen 300% - 400% -->
    <div class="AdsScreen">
        <div class="promotion">
            <h2>¡Promoción!</h2>
        </div>
        <div class="promotionTitle">
            <h2>Hard Rock Hotel</h2>
        </div>
        <div class="promotionDate">
            <h3>01/12/16 - Deluxe Gold - $5,886 MXN</h3>            
        </div>
        <div class="promotionJoin">
            <button><a href="">Ver detalles</a></button>
        </div>        
    </div>
    <!-- Fourth Screen 400% - 500% -->
   <div class="secundaryAds">
        <div class="secundaryHead">
            <h2>Tenemos lo que estás buscando</h2>
            <h3>Lo más buscado</h3>
        </div>
        <div class="Ads">
            <!--<div class="container">-->
                <div class="containerCard">
                <div class="column">
                    <div class="post-module">
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day">27</div>
                                <div class="month">Mar</div>
                            </div>
                            <img src="img/FiestaAmericana.jpg"/>
                        </div>
                        <div class="post-content">
                            <div class="category">-35%</div>
                            <h1 class="title">Grand Fiesta Americana</h1>
                            <h2 class="sub_title">Coral Beach Cancun</h2>
                            <p class="description">Habitación Gran Club, vistas al océano
                                Suite, 1 cama King size (Master)
                            </p>
                            <div class="post-meta"><span class="timestamp"><i class="fa fa-clock-">o</i> 3 días restantes</span><span class="comments"><i class="fa fa-comments"></i><a href="#"> 5 comentarios</a></span></div>
                        </div>
                    </div>
                </div>
            </div>
             
             <div class="containerCard">
                <div class="column">
                    <div class="post-module">
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day">27</div>
                                <div class="month">Mar</div>
                            </div>
                            <img src="img/JugoDeLimon.jpg"/>
                        </div>
                        <div class="post-content">
                            <div class="category">-12%</div>
                            <h1 class="title">Jugo De Limón</h1>
                            <h2 class="sub_title">Restaurant único</h2>
                            <p class="description">Una cevichería de estilo mexicano con la mejor y más popular cocina de la costa mexicana, además de otros platos tradicionales.</p>
                            <div class="post-meta"><span class="timestamp"><i class="fa fa-clock-">o</i> 2 días</span><span class="comments"><i class="fa fa-comments"></i><a href="#"> 2 comentarios</a></span></div>
                        </div>
                    </div>
                </div>
            </div>
              
            <div class="containerCard">
                <div class="column">
                    <div class="post-module">
                        <div class="thumbnail">
                            <div class="date">
                                <div class="day">27</div>
                                <div class="month">Mar</div>
                            </div>
                            <img src="img/Mandala.jpg"/>
                        </div>
                        <div class="post-content">
                            <div class="category">6 MSI</div>
                            <h1 class="title">Mandala Beach Club</h1>
                            <h2 class="sub_title">Despierta tus sentidos</h2>
                            <p class="description">Las mejores fiestas en la playa ocurren en un solo lugar: Mandala Beach Club, el Mar Caribe, bikinis, bebidas tropicales.</p>
                            <div class="post-meta"><span class="timestamp"><i class="fa fa-clock-">o</i> 7 días</span><span class="comments"><i class="fa fa-comments"></i><a href="#"> 10 comentarios</a></span></div>
                        </div>
                    </div>
                </div>
            </div>            
            </div>
        </div>            
    <!-- Fifth Screen 500% - 600% -->
    
    <!-- Sixth Screen 600% - 700% -->
    <div class="monthScreen">
        <div class="monthHead">
            <h2>Recomendación de la semana</h2>                       
        </div>
        <div class="monthContent">
            <div class="fx-card">
                <div class="fx-card__img">
                    <img src="img/Interceramic.jpg" alt=""/>
                </div>
                <div class="fx-card__info">
                    <h1 class="fx-card__title">Interceramic Sucursal Portillo</h1>
                    <p class="fx-card__text">Dale a tu hogar el lugar que merece.
                    En pisos y azulejos Interceramic tiene todo para remodelar o ampliar tu hogar.
                    Interceramic, Simplemente lo mejor.</p>
                    <p>Después de más de 35 años en el mercado, Interceramic se ha convertido en líder indiscutible, no solamente como fabricante, sino también como distribuidor de pisos y azulejos cerámicos, muebles de baño, materiales para instalación y piedra natural.</p>
                </div>
            </div>
            <div class="fx-card">
                <div class="fx-card__img">
                    <img src="img/Universidad.jpg" alt=""/>
                </div>
                <div class="fx-card__info">
                    <h1 class="fx-card__title">Universidad Politécnica de Quintana Roo</h1>
                    <p class="fx-card__text">¡En tiempos de cambios seamos pro activos. Somos una Universidad con desarrollo inteligente, sostenible e integrador; efectiva y eficiente. Los caminos hay que construirlos, los horizontes hay que alcanzarlos y el futuro hay que soñarlo. Integrate al progreso continuo, las cosas no ocurren por si mismas, ¡Hagamos que sucedan!.</p>                    
                </div>
            </div>
        </div>
    </div>
    <!-- Sixth Screen 700% - 800% -->
    <div class="newslatter">
       <div class="newslatterHead">
           <h3>Sucribete</h3>
           <h4>Recibe las mejores ofertas semanales</h4>
       </div>
        <input type="text" placeholder="Correo: " id="Correo">
        <input type="button" value="Suscribirse" id="Suscribirse">
    </div>
    
    <footer> 
        <div class="container">
            <div class="col-md-6 info">
                <img src="img/DIRCUNblanco.png" alt="" class="img-responsive">
                <p>Dircun, la plataforma que entiende lo que quieres ver y tener, conoce, visita, consume, apoya, opina y participa.</p>
            </div>
            <div class="col-md-6 text-center social">
                <h2>Get in touch!</h2>
                <p><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i>
                <i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i>
                <i class="fa fa-linkedin-square fa-2x" aria-hidden="true"></i>
                <i class="fa fa-pinterest-square fa-2x" aria-hidden="true"></i></p>
            </div>
        </div>
    </footer>
    <!-- Sixth Screen 600% - 700% -->
    <div class="footerCopy">
        <div class="container">
           <div class="copy col-md-6 col-xs-12">
               <p>Dircun © 2016 / All Rights Reserved</p> 
           </div>
           <div class="nav col-md-6 col-xs-12 ">
               <p>Inicio / Acerca de / Miembros</p>
           </div>                    
        </div>           
    </div>
    <!-- Scripts -->
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-2.2.4.min.js"><\/script>')</script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>    
</body>
</html>