<?php
    $username = 'root';
    $password = '';
    $connection = new PDO(
        'mysql:host=localhost;dbname=prtn',
        $username,
        $password
    );

    $connection->setAttribute(
        PDO::ATTR_ERRMODE,
        PDO::ERRMODE_EXCEPTION
    );
?>