<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Buscar: Compras - Dircun</title>
    <meta name="author" content="Reef Studios">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">   
    <link rel='shortcut icon' type='image/x-icon' href='favicon.ico'/>
    <!-- Hojas de estilos -->            
    <link rel="stylesheet" href="css/styleBusquedas.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">    
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">
</head>
<body>
    <div class="searchScreen">
        <div class="logo">
            <a href="index.php"><img src="img/DirCun.gif" alt="" class="img-responsive"></a>
        </div>
        <div class="serchContent">
            <div class="searchHead">
                <h2>Resultados para: <span> Compras</span></h2>
                <hr>
            </div>
            <div class="searchContainer">
                <div class="element wow fadeInUp" data-wow-delay="0.2s">
                    <div class="image">
                        <img src="img/AmorAmor.png" alt="">
                    </div>
                    <div class="title">
                        <h2>Amor Amor Fashion Bikinis</h2>
                        <p>Una pequeña boutique inspirada en el amor por el mar. 
                            Trajes de baño coloridos que contrastan con la piel dorada por el sol; fusión de tendencias y estilos de alta moda con inspiración latina.
                        <br><a href="AmorAmor.php" class="more">Más información</a> </p> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.4s">
                    <div class="image">
                        <img src="img/Interceramic.jpg" alt="">
                    </div>
                    <div class="title">
                        <h2>Interceramic</h2>
                        <p> Dale a tu hogar el lugar que merece.
                        En pisos y azulejos Interceramic tiene todo para remodelar o ampliar tu hogar.
                        Interceramic, Simplemente lo mejor.
                        Después de más de 35 años en el mercado, Interceramic se ha convertido en líder indiscutible, no solamente como fabricante, sino también como distribuidor de pisos y azulejos cerámicos, muebles de baño, materiales para instalación y piedra natural. 
                        <br> <a href="Interceramic.php" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.6s">
                    <div class="image">
                        <img src="img/Dormimundo.JPG" alt="">
                    </div>
                    <div class="title">
                        <h2>Dormimundo</h2>
                        <p> Para Dormimundo si el descanso es personal y cada cliente duerme de una manera diferente, el colchón debe ser la base de ese descanso, por eso personaliza su amplia gama de colchones para que los clientes puedan encontrar y escoger entre un amplio abanico de opciones cual es la mejor para su reposo
                        <br> <a href="Dormimundo.php" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>
                <div class="element wow fadeInUp" data-wow-delay="0.6s">
                    <div class="image">
                        <img src="img/Kipling.JPG" alt="">
                    </div>
                    <div class="title">
                        <h2>Kipling Plaza Malecón Americas</h2>
                        <p> Kipling se ha vuelto el accesorio de moda de más de 35 millones de mujeres en más de 60 países.
                        Desde su inicio la imagen de la marca es un chango con mucha personalidad que se encuentra en las bolsas, maletas y en todos los demás accesorios.
                        <br> <a href="" class="more">Más información</a> </p><br> 
                    </div>                    
                </div>  
                <div class="endSearch">
                    <br>
                    <h2>- Fin de las busquedas -</h2>
                    <br>
                </div>                
            </div>            
        </div>        
    </div>
    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="dist/wow.js"></script>
    <script>
        new WOW().init();
    </script> 
</body>
</html>