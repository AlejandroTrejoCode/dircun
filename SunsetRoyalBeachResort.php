100%!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Dircun</title>
    <link rel="stylesheet" href="">
    <!-- Hojas de estilos -->
    <link rel="stylesheet" href="css/styleEstablecimiento.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!--<link rel="stylesheet" href="css/generics.css">-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700,900" rel="stylesheet"> 
    <link href="animate.css" rel="stylesheet">
</head>
<body>
<div class="screen">
<div class="container">
<!--Container-->
<div class="contentEstablecimiento col-xs-12 col-md-8"> 
<!--Header-->
    <div class="headerEstablecimiento"><h1>Sunset Royal Beach Resort</h1></div> 
<!--Image-->
    <div class="imagesEstablecimiento col-xs-12 col-md-6 img-responsive"> 
        <img src="http://agenciadeviajescubanatravel.com.mx/wp-content/uploads/2013/01/Cancun-Royal-Sunset-Playa.jpg" alt="Cancún es bello">
    </div>
<!--Description-->
    <div class="descriptionEstablecimiento col-xs-12 col-md-6"> 
        <p>
            Este complejo con todo incluido tranquilo se encuentra en una playa de arena blanca, a 2,6 km del Acuario Interactivo de Cancún y a 8 km del Museo Maya de Cancún. 
        </p>
    </div>

<!--Maps-->
    <div class="mapEstablecimiento col-xs-12 col-md-12"> 
       <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1ses-419!2s!4v1480380517880!6m8!1m7!1smJvy6ZLcARgAAAQz5Grd1A!2m2!1d21.12566108237552!2d-86.75180528532314!3f78.08!4f0!5f0.7820865974627469" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
        
    </div>
<!--Contact-->
    <div class="contactEstablecimiento col-xs-12 col-md-12"> 
       <div class="contact col-xs-3 col-md-3"> 
           <label>Contacto</label>
       </div>
    <!--<<div class="email col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-envelope"></span>
            <email>contacto@contacto.com</email>
        </div>-->
        <div class="telephone col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-earphone"></span>
            <tel> 01 998 881 4500</tel>
        </div>
        <div class="facebook col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-thumbs-up"></span>
            <a href="https://www.facebook.com">Facebook</a>
        </div>
        <div class="twitter col-xs-3 col-md-3">
            <span class="glyphicon glyphicon-heart"></span>
            <a href="https://www.twitter.com">Twitter</a>
        </div>       
    </div>
</div> 
</div>
</div>


<div class="scrReviews col-xs-12 col-md-12">
   <div class="contReviews col-xs-12 col-md-12">
       <div class="reviews col-xs-11 col-md-11">
       <div class="headerReviews"><h1>Reviews</h1></div> 
        <div class="textReviews col-xs-10 col-md-10">
           <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-width="100%" data-numposts="5"></div>
        </div>
       </div>
   </div>
</div>

</body>
</html>